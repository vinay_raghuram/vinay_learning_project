$(document).ready(
	function(){
		var requestURI = 'http://localhost:8080/api/students/top/local/';
		var topTenTemplate = _.template('<tr><td><%- sid %></td><td><%- name %></td><td><%- cumulative %></td></tr>');
		var intervalCount = 1; //keeps track of interval to be pulled

		var topTenSuccessHandler = function(data){
			for (var count = 0; count < data.length; count++){
				var newEntry = topTenTemplate(data[count]);
				$('#topten-table').append(newEntry);
			}
			intervalCount ++;
		};
		
		$.getJSON(requestURI + String(intervalCount), topTenSuccessHandler)

		$('#topTenButton').click(function(){
			$.getJSON(requestURI + String(intervalCount), topTenSuccessHandler);
			var y = $('#scrolltopten').scrollTop();
			$('#scrolltopten').scrollTop(y + 240);
		});
	}
);

