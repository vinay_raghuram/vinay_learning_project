#!/usr/bin/env/python

import json
import names
import pymongo
import random
import sys

NUMBER_OF_STUDENTS = int(sys.argv[1])
ETHNICITIES = [u'African American', 
			   u'White', 
			   u'Asian/Pacific Islander', 
			   u'Hispanic', 
			   u'Native American']

COURSES_OFFERED = [u'math', 
                   u'science', 
                   u'history', 
                   u'english']

GRADES = { u'F' : 0.0,
           u'D' : 1.0,
           u'C' : 2.0,
           u'B' : 3.0,
           u'A' : 4.0 }

def coin_toss():
	"""
	Performs a simple coin toss
	"""
	return random.randrange(2)

def generate_student_info(student_id=None):
	"""
	Returns a tuple containing a randomly generated name along with gender,
	age, ethnicity, and student id number
	"""
	ethnicity = random.choice(ETHNICITIES)
	male = coin_toss()
	if male:
		name = names.get_full_name(gender='male')
		return name, {
			u'age' : random.randrange(14, 19),
			u'gender' : u'male', 
			u'sid' : student_id,
			u'ethnicity' : ethnicity
			}
	else:
		name = names.get_full_name(gender='female')
		return name, {
			u'age' : random.randrange(14, 19),
			u'gender' : u'female', 
			u'sid': student_id, 
			u'ethnicity' : ethnicity
			}

def generate_letter_grade():
	"""
	Generates a letter grade from a random GPA
	"""
	gpa = 4.0*random.random()
	if 0 <= gpa < 0.7:
		return u'F'
	elif 0.7 <= gpa < 1.3:
		return u'D'
	elif 1.3 <= gpa < 2.3:
		return u'C'
	elif 2.3 <= gpa < 3.3:
		return u'B'
	else:
		return u'A'

def grade_to_gpa(grade):
	"""
	Converts a letter grade to a float
	"""
	return GRADES[grade]

def generate_grades():
	"""
	Generates Cumulative GPA and grades from the most recent semester
	"""
	grades = { course : generate_letter_grade() for course in COURSES_OFFERED }
	gpa = 0.0
	for course in grades:	gpa += grade_to_gpa(grades[course])
	grades[u'cumulative'] = gpa/len(COURSES_OFFERED)
	return grades

def generate_data():
	"""
	"""
	students = []
	for n in range(NUMBER_OF_STUDENTS):
		name, info = generate_student_info(n)
		grades = generate_grades()
		profile = dict({u'name' : name}, **info)
		profile.update(grades)
		students.append(profile)

	students.sort(key=lambda entry: entry[u'cumulative'])
	return students



def main():
	"""
	Generates random student data
	"""
	students = generate_data()
	with open('student_data.json', 'w') as student_data:
		student_data.write(json.JSONEncoder().encode(students))




if __name__ == '__main__':
	main()



























