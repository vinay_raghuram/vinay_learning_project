$(document).ready(
	function(){
		var genderPieChart = function(data){
			var maleCount = data['gender']['male'];
			var femaleCount = data['gender']['female'];

			$(function(){
				var chartData = [{name : 'Male', value : maleCount, color : '#99ccff'}, 
				                 {name : 'Female', value : femaleCount, color : '#ffccff'}];

				var chart = new iChart.Pie2D({
					render : 'genderPieChart',
					data   : chartData,
					title  : 'Student Body by Gender'
				});
				chart.draw();
			});
		};

		var ethnicityPieChart = function(data){
			var eth = data['ethnicity'];
			var ethnicityCount = { 'African American' : eth['African American'],
			                       'White' : eth['White'],
			                       'Asian/Pacific Islander' : eth['Asian/Pacific Islander'],
			                       'Hispanic' : eth['Hispanic'],
			                       'Native American' : eth['Native American'] };


			$(function(){
				var ethnicities = _.keys(ethnicityCount);
				var chartData = [{name : "African American", value : ethnicityCount['African American'], color : '#339933'},
								 {name : "White", value : ethnicityCount['White'], color : '#3366cc'},
								 {name : "Asian/Pacific Islander", value : ethnicityCount['Asian/Pacific Islander'], color : '#ff3300'},
								 {name : "Hispanic", value : ethnicityCount['Hispanic'], color : '#cccc00'},
								 {name : "Native American", value : ethnicityCount['Native American'], color : '#990099'}];

				var chart = new iChart.Pie2D({
					render : 'genderPieChart',
					data   : chartData,
					title  : 'Student Body by Ethnicity'
				});
				chart.draw();
			});
		};

		//##################################

		var request_url = "http://localhost:8080/api/students/data/local";
		
		//##################################
		var successHandler = function(data){
			var student_data = data;
			genderPieChart(student_data);


			$('#toggleGenderPieChart').click(function(){
				genderPieChart(student_data);
			});

			$('#toggleEthnicityPieChart').click(function(){
				ethnicityPieChart(student_data);
			});
		}

		$.getJSON(request_url, successHandler);
	}
);






