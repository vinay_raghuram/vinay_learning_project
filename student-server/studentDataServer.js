var bodyParser = require('body-parser')
var events = require('events'); //events module
var express = require('express'); //node.js express
var fs = require('fs'); //File-system module
var mongoose = require('mongoose'); //DB stuff
var _ = require('underscore'); //underscore for sorting

var app = express();
var port = process.env.port || 8080; //set standart HTTP port

//create a new instance or router
var router = express.Router();

//connect to mongoDB cloud
mongoose.connect('mongodb://cisco-cmx:cisco123@ds061325.mongolab.com:61325/vinay_student_db');
//mongoose models for student and classroom objects
var Student = require('./app/models/student'); //student object for each student

//routes for API go here
//get list of students from local json
var studentsList = require('./student_data.json');
var studentsList_sortedByGPA = _.sortBy(studentsList, function(entry){return -1*entry['cumulative']});

var studentAggregateData = {
	"gender" : {
		"male" : 0,
		"female" : 0
	},
	"ethnicity" : {
		"White" : 0,
		"African American" : 0,
		"Asian/Pacific Islander" : 0,
		"Native American" : 0,
		"Hispanic" : 0
	},
	"grades" : {
		"math" : {"A" : 0, "B" : 0, "C" : 0, "D" : 0, "F" : 0},
		"science" : {"A" : 0, "B" : 0, "C" : 0, "D" : 0, "F" : 0},
		"english" : {"A" : 0, "B" : 0, "C" : 0, "D" : 0, "F" : 0},
		"history" : {"A" : 0, "B" : 0, "C" : 0, "D" : 0, "F" : 0},
	}
};

//compile aggregate data
var collectData = function(){
	var student;
	for (var j = 0; j < studentsList.length; j++){
		student = studentsList[j];
		if (student['gender'] == 'male') studentAggregateData['gender']['male']++;
		else studentAggregateData['gender']['female'] ++;

		studentAggregateData['ethnicity'][student['ethnicity']]++;
		studentAggregateData['grades']['math'][student['math']]++;
		studentAggregateData['grades']['science'][student['science']]++;
		studentAggregateData['grades']['english'][student['english']]++;
		studentAggregateData['grades']['history'][student['history']]++;
	}
};
collectData();


//GET hello world api (for testing)
router.route('/hello').get(function(req, res){
	res.json({"output" : "hello, world!"});
});

//GET a list of all students, in no particular order
router.route('/students').get(function(req, res){
	Student.find(function(err, students){
		if (err) {res.send(err);}
		res.json(students);
	});
});

//GET a list of students (file stored locally)
router.route('/students/local').get(function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.json(studentsList);
});

//GET a list of students (file stored locally)
router.route('/students/top/local').get(function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");	
	res.json(studentsList_sortedByGPA);
});

//GET an interval of 10 students sorted by GPA
router.route('/students/top/local/:interval_no').get(function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	var interval_no = req.params.interval_no;
	var last_index = 10*interval_no;
	var retval = [];
	for (var i = last_index - 10; i < last_index; i ++){
		retval.push(studentsList_sortedByGPA[i]);
	}
	res.json(retval);		
});

//GET aggregate data for the classroom (file stored locally)
router.route('/students/data/local').get(function(req, res){
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "X-Requested-With");
	res.json(studentAggregateData);
});


//register routes
app.use('/api', router);

//start the server
app.listen(port);
console.log("server online");