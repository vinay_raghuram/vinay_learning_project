var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StudentSchema = new Schema({
	"_id" : {
		"$old" : String
	},
	"cumulative" : Number,
	"name" : String,
	"gender" : String,
	"age" : Number,
	"english" : String,
	"math" : String,
	"sid" : Number,
	"history" : String,
	"ethnicity" : String,
	"science" : String,

});

var Student = mongoose.model('Student', StudentSchema);
module.exports = Student;

